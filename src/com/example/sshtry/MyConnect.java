package com.example.sshtry;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class MyConnect extends AsyncTask<String, Void, String> {
	
	View view;
	String functionToExec;
	private final static String TAG = "RDEBUG"; 

	@Override
	protected String doInBackground(String... arg0) {
		
		List<String> output = new ArrayList<String>();
		Context contexto = view.getContext();
		
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(contexto);
		
		String user = sharedPref.getString("ssh_username", "");
		String passwd = sharedPref.getString("ssh_password", "");
		String host="";
		String command1 = "echo " + passwd + " | sudo -S service minidlna stop";
		String command2 = "echo " + passwd + " | sudo -S minidlna -R";
		String command3 = "echo " + passwd + " | sudo -S shutdown -h now";
		Integer port = Integer.parseInt(sharedPref.getString("ssh_port", "22"));
		
		Log.i(TAG, "usuario_ " + user);
		Log.i(TAG, "password_ " + passwd);
		Log.i(TAG, "port_ " + port.toString());
		
		NetworkUtils nu = new NetworkUtils(contexto);
		Integer connMode = nu.getConnectionMode();
		
		if (connMode == NetworkUtils.GSM_MODE){
			host = sharedPref.getString("ssh_ext_hostname", "");
		} else if (connMode == NetworkUtils.WIFI_MODE){
			host = sharedPref.getString("ssh_lan_hostname", "");
		}
		
		Log.i(TAG, "hostname_ " + host);
		
		try {
			JSch ssh = new JSch();
			
			Session session = ssh.getSession(user, host, port);
			session.setPassword(passwd);
			
			Properties prop = new Properties();
			prop.put("StrictHostKeyChecking", "no");
			session.setConfig(prop);
			
			session.connect();
			
			Channel channel=session.openChannel("exec");
			
			if (functionToExec=="stop"){
				((ChannelExec)channel).setCommand(command1);
			}
			
			if (functionToExec=="start"){
				((ChannelExec)channel).setCommand(command2);
			}
			
			if (functionToExec=="shutdown"){
				((ChannelExec)channel).setCommand(command3);
			}
			
			InputStream in=channel.getInputStream();

			channel.connect();

			byte[] tmp=new byte[1024];
			while(true){
				while(in.available()>0){
					int i=in.read(tmp, 0, 1024);
					if(i<0)break;
					output.add(new String(tmp, 0, i));
				}
				if(channel.isClosed()){
					System.out.println("exit-status: "+channel.getExitStatus());
					break;
				}
				try{
					Thread.sleep(1000);
				}catch(Exception ee){
					
				}
			}
			channel.disconnect();
			session.disconnect();
			
		} catch(Exception e) {
			System.out.println(e);
		}
		String[] stringOutput = new String[output.size()];
		output.toArray(stringOutput);
		return Arrays.toString(stringOutput);
	}
	
	public void setView(View viewItem){
		view = viewItem;
	}
	
	public void setFunctionToExec(String functionString){
		functionToExec = functionString;
	}
	
	protected void onPostExecute(String result){
		Toast.makeText(view.getContext(), "Hecho.", Toast.LENGTH_LONG).show();
	}

}
